package com.bantic.htec.ui.posts

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bantic.htec.data.entities.Post
import com.bantic.htec.databinding.ItemPostBinding

class PostAdapter(private val listener: PostItemListener) : RecyclerView.Adapter<PostViewHolder>() {

    interface PostItemListener {
        fun onPostClicked(post: Post)
    }

    private val items = ArrayList<Post>()

    fun setItems(listOfPosts: ArrayList<Post>) {
        items.clear()
        items.addAll(listOfPosts)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val binding: ItemPostBinding =
            ItemPostBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PostViewHolder(binding, listener)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) =
        holder.bind(items[position])

}

class PostViewHolder(
    private val itemBinding: ItemPostBinding,
    private val listener: PostAdapter.PostItemListener
) : RecyclerView.ViewHolder(itemBinding.root),
    View.OnClickListener {

    private lateinit var post: Post

    init {
        itemBinding.root.setOnClickListener(this)
    }

    fun bind(item: Post) {
        this.post = item
        itemBinding.title.text = item.title
        itemBinding.description.text = item.body
    }

    override fun onClick(v: View?) {
        listener.onPostClicked(post)
    }
}
