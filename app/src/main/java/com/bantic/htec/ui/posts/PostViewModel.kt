package com.bantic.htec.ui.posts

import android.content.Context
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import com.bantic.htec.data.entities.Post
import com.bantic.htec.data.repository.PostsRepository
import com.bantic.htec.data.work.RemoveDataWork
import com.bantic.htec.utils.Resource
import java.util.concurrent.TimeUnit

class PostViewModel @ViewModelInject constructor(
    private val repository: PostsRepository
) : ViewModel() {

    private val loadTrigger = MutableLiveData(Unit)

    val posts: LiveData<Resource<List<Post>>> = loadTrigger.switchMap {
        repository.getPosts()
    }

    fun refreshListOfPosts() {
        loadTrigger.value = Unit
    }

    fun clearDatabase(context: Context?) {
        val request = OneTimeWorkRequestBuilder<RemoveDataWork>()
            .setInitialDelay(5, TimeUnit.MINUTES)
            .build()

        context?.let {
            WorkManager.getInstance(it).cancelAllWork()
            WorkManager.getInstance(it).enqueue(request)
        }
    }

}