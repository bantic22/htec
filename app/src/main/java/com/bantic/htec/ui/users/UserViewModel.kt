package com.bantic.htec.ui.users

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import com.bantic.htec.data.entities.Post
import com.bantic.htec.data.entities.User
import com.bantic.htec.data.repository.PostsRepository
import com.bantic.htec.utils.Resource

class UserViewModel @ViewModelInject constructor(
    private val repository: PostsRepository
) : ViewModel() {

    val removeClicked: MutableLiveData<Boolean> = MutableLiveData(false)

    private val _id = MutableLiveData<Int>()

    private val _users = _id.switchMap { id ->
        repository.getUsers(id)
    }
    val user: LiveData<Resource<User>> = _users

    // search in database
    fun setUserId(id: Int) {
        _id.value = id
    }

    fun removePost(post: Post) {
        repository.removePostFromCache(post.id)
        removeClicked.postValue(true)
    }

}