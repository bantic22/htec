package com.bantic.htec.ui.posts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.bantic.htec.R
import com.bantic.htec.data.entities.Post
import com.bantic.htec.databinding.PostFragmentBinding
import com.bantic.htec.utils.Resource
import com.bantic.htec.utils.autoCleared
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class PostFragment : Fragment(), PostAdapter.PostItemListener {

    private var binding: PostFragmentBinding by autoCleared()
    private val viewModel: PostViewModel by viewModels()
    private lateinit var adapter: PostAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = PostFragmentBinding.inflate(inflater, container, false)
        binding.postViewModel = viewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupObservers()
    }

    private fun setupRecyclerView() {
        adapter = PostAdapter(this)
        binding.postsRv.adapter = adapter
    }

    private fun setupObservers() {
        viewModel.posts.observe(viewLifecycleOwner, Observer {
            Timber.e("status %s", it.status)
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    binding.refreshLayout.isRefreshing = false
                    Timber.e("list size: %s", it.data?.size)
                    if (!it.data.isNullOrEmpty()) {
                        adapter.setItems(ArrayList(it.data))
                        binding.noDataText.visibility = View.GONE
                        binding.postsRv.visibility = View.VISIBLE
                        viewModel.clearDatabase(context)

                    } else {
                        binding.noDataText.visibility = View.VISIBLE
                        binding.postsRv.visibility = View.GONE
                    }
                }
                Resource.Status.ERROR -> {
                    binding.refreshLayout.isRefreshing = false
                    binding.noDataText.visibility = View.VISIBLE
                    binding.postsRv.visibility = View.GONE
                }

                Resource.Status.LOADING -> {
                    //Handled by databinding
                }
            }
        })
    }

    override fun onPostClicked(post: Post) {
        findNavController().navigate(
            R.id.action_postFragment_to_userFragment,
            bundleOf(
                "postItem" to post
            )
        )
    }

}

