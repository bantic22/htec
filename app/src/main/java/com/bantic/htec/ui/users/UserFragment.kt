package com.bantic.htec.ui.users

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.bantic.htec.data.entities.Post
import com.bantic.htec.databinding.UserFragmentBinding
import com.bantic.htec.utils.Resource
import com.bantic.htec.utils.autoCleared
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UserFragment : Fragment() {

    private var binding: UserFragmentBinding by autoCleared()
    private val viewModel: UserViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = UserFragmentBinding.inflate(inflater, container, false)
        binding.userViewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getParcelable<Post>("postItem")?.let {
            viewModel.setUserId(it.userId)
            binding.post = it
        }
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.user.observe(viewLifecycleOwner, Observer { result ->
            when (result.status) {
                Resource.Status.SUCCESS -> {
                    result.data?.let {
                        binding.user = it
                        binding.progressBar.visibility = View.GONE
                        binding.detailsCl.visibility = View.VISIBLE
                    }
                }

                Resource.Status.ERROR ->
                    Toast.makeText(activity, result.message, Toast.LENGTH_SHORT).show()

                Resource.Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.detailsCl.visibility = View.GONE
                }
            }
        })

        viewModel.removeClicked.observe(viewLifecycleOwner, {
            if (it) {
                activity?.onBackPressed()
            }
        })
    }

}