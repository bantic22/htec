package com.bantic.htec.data.repository

import com.bantic.htec.data.local.PostDao
import com.bantic.htec.data.remote.PostsRemoteDataSource
import com.bantic.htec.utils.performGetOperation
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class PostsRepository @Inject constructor(
    private val remoteDataSource: PostsRemoteDataSource,
    private val localDataSource: PostDao
) {

    fun getPosts() = performGetOperation(
        databaseQuery = { localDataSource.getAllPosts() },
        networkCall = { remoteDataSource.getPosts() },
        saveCallResult = { localDataSource.insertPosts(it) }
    )

    fun getUsers(id: Int) = performGetOperation(
        databaseQuery = {
            localDataSource.getUserById(id)
        },
        networkCall = {
            remoteDataSource.getUser(id)
        },
        saveCallResult = {
            localDataSource.insertUsers(listOf(it))
        }
    )

    fun removePostFromCache(postId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            localDataSource.removePost(postId)
        }
    }

    fun dropDatabase() {
        CoroutineScope(Dispatchers.IO).launch {
            localDataSource.removePostsTable()
        }
    }
}