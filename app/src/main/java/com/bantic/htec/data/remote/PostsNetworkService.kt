package com.bantic.htec.data.remote

import com.bantic.htec.data.entities.Post
import com.bantic.htec.data.entities.User
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface PostsNetworkService {

    @GET("posts")
    suspend fun getAllPosts(): Response<List<Post>>

    @GET("users/{id}")
    suspend fun getUserData(@Path("id") id: Int): Response<User>
}