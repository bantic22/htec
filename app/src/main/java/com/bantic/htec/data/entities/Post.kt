package com.bantic.htec.data.entities

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "posts")
data class Post(

    @ColumnInfo(name = "userId")
    @SerializedName("userId") val userId: Int,

    @PrimaryKey
    @ColumnInfo(name = "id")
    @SerializedName("id") val id: Int,

    @ColumnInfo(name = "title")
    @SerializedName("title") val title: String,

    @ColumnInfo(name = "body")
    @SerializedName("body") val body: String
) : Parcelable