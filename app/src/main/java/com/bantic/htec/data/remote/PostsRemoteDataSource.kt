package com.bantic.htec.data.remote

import javax.inject.Inject

class PostsRemoteDataSource @Inject constructor(
    private val postsNetworkService: PostsNetworkService
) : BaseDataSource() {

    suspend fun getPosts() = getResult { postsNetworkService.getAllPosts() }
    suspend fun getUser(userId: Int) = getResult { postsNetworkService.getUserData(userId) }
}