package com.bantic.htec.data.work

import android.content.Context
import androidx.hilt.Assisted
import androidx.hilt.work.WorkerInject
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.bantic.htec.data.repository.PostsRepository
import timber.log.Timber

class RemoveDataWork @WorkerInject constructor(
    @Assisted appContext: Context,
    @Assisted workerParams: WorkerParameters,
    private val repository: PostsRepository
) : Worker(appContext, workerParams) {


    override fun doWork(): Result {
        Timber.d("WorkManager clean database cache")
        repository.dropDatabase()
        return Result.success()
    }
}