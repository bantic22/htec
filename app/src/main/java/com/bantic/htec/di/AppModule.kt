package com.bantic.htec.di

import android.content.Context
import com.bantic.htec.data.local.AppDatabase
import com.bantic.htec.data.local.PostDao
import com.bantic.htec.data.remote.PostsNetworkService
import com.bantic.htec.data.remote.PostsRemoteDataSource
import com.bantic.htec.data.repository.PostsRepository
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideOkHttpClient(cache: Cache): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder().cache(cache).addInterceptor(interceptor)
            .addInterceptor { chain: Interceptor.Chain ->
                val original: Request = chain.request()
                val requestBuilder: Request.Builder = original.newBuilder()
                    .addHeader("Accept", "Application/JSON")
                val request: Request = requestBuilder.build()
                chain.proceed(request)
            }
        return client.build()
    }

    @Singleton
    @Provides
    fun provideCache(@ApplicationContext context: Context): Cache =
        Cache(context.cacheDir, 5 * 1024 * 1024)

    @Singleton
    @Provides
    fun provideRetrofit(httpClient: OkHttpClient, gson: Gson): Retrofit = Retrofit.Builder()
        .baseUrl("https://jsonplaceholder.typicode.com")
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(httpClient)
        .build()

    @Provides
    fun provideGson(): Gson = GsonBuilder()
        .create()

    @Provides
    fun providePostsService(retrofit: Retrofit): PostsNetworkService =
        retrofit.create(PostsNetworkService::class.java)

    @Singleton
    @Provides
    fun providePostsRemoteDataSource(postsNetworkService: PostsNetworkService) =
        PostsRemoteDataSource(postsNetworkService)

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context) =
        AppDatabase.getDatabase(appContext)

    @Singleton
    @Provides
    fun providePostDao(db: AppDatabase) = db.postDao()

    @Singleton
    @Provides
    fun provideRepository(
        remoteDataSource: PostsRemoteDataSource,
        localDataSource: PostDao
    ) =
        PostsRepository(remoteDataSource, localDataSource)

}