package com.bantic.htec

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import com.bantic.htec.ui.MainActivity
import com.bantic.htec.ui.posts.PostViewHolder
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class PostsFragmentTest {

    @get:Rule
    val activityRule = ActivityTestRule(MainActivity::class.java, true, true)

    @Test
    fun testPostsFragment() {
        //Test UI
        onView(withId(R.id.refresh_layout)).check(matches(isDisplayed()))
        onView(withId(R.id.no_data_text)).check(matches(withEffectiveVisibility(Visibility.GONE)))
        onView(withId(R.id.progress_bar)).check(matches(withEffectiveVisibility(Visibility.GONE)))
    }

    @Test
    fun testAdapter() {
        //Test list
        onView(withId(R.id.posts_rv)).check(matches(isDisplayed()))
        onView(withId(R.id.posts_rv))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<PostViewHolder>(5, click())
            )
    }
}