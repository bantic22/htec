package com.bantic.htec

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.bantic.htec.ui.MainActivity
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Test
    fun testMainActivity() {
        ActivityScenario.launch(MainActivity::class.java)

        // Test toolbar
        onView(withId(R.id.toolbar)).check(matches(isDisplayed()))
        onView(withParent(withId(R.id.toolbar))).check(matches(withText("HTEC Users")))
    }
}